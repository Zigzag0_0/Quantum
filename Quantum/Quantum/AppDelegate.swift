//
//  AppDelegate.swift
//  Quantum
//
//  Created by PAMacBook on 26.04.18.
//  Copyright © 2018 Zigzag. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
       
        window = UIWindow()
        window?.makeKeyAndVisible()
        
        window?.rootViewController = MainVC()
        return true
    }

}

